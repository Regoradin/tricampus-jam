using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plant : MonoBehaviour
{
    public bool doesReinforce;
    public int restorationFactor;

    public bool doesRestoreHealth;
    private Health playerHealth;
    public float healFactor;

    private GrowthGrid grid;

    //public int areaRequirements;

    private void Start()
    {
        if (grid == null)
        {
            grid = GrowthGrid.ActiveGrid;
        }
    }

    private void Update()
    {
        if (doesReinforce)
        {
            FortifyArea();
        }
        if (doesRestoreHealth)
        {
            HealPlayer();
            healFactor = 0;
        }
    }

    public void HealPlayer()
    {
        playerHealth.Heal(healFactor);
    }

    public void FortifyArea()
    {
        grid.SetBurstAtWorldSpacePosition(transform.position, restorationFactor, 1f);
    }
}
