using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResurrectionAbility : MonoBehaviour
{
    //public Resurrectable selectedResurrectable;
    public int selectedResurrectableIndex {get; private set;}
    public List<Resurrectable> availableResurrectables;

    [Range(0, 50)]
    public float resurrectionRange;

    private Health playerHealth;
    private GrowthGrid grid;
    private Animator anim;

    private void Awake()
    {
        playerHealth = GetComponent<Health>();
        anim = GetComponentInChildren<Animator>();
    }

    private void Start()
    {
        grid = GrowthGrid.ActiveGrid;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit);
            
            Vector3 resurrectionPosition = hit.point;
            TryResurrectingCurrentlySelected(resurrectionPosition);
        }

        //select active resurrectable;
        if (Input.inputString != "")
        {
            try
            {
                int number = System.Int32.Parse(Input.inputString);
                //selectedResurrectable = availableResurrectables[number - 1];
                if ((number - 1) >= availableResurrectables.Count)
                {
                    throw new System.Exception();
                }
                selectedResurrectableIndex = number - 1;
            }
            catch{} //there are a lot of reasons why this wouldn't work, none of which are particularly problematic (not numeric input, not enough resurrectables)
        }
    }

    private void TryResurrectingCurrentlySelected(Vector3 position)
    {
        Resurrectable resurrectable = availableResurrectables[selectedResurrectableIndex];
            
        if (playerHealth.health < resurrectable.healthCost ||
            grid.GetGridValueAtWorldSpacePosition(position) <= 0 ||
            Vector3.Distance(position, transform.position) > resurrectionRange)
        {
            return;
        }

        GameObject newObj = Instantiate(resurrectable.gameObject, position, Quaternion.identity);
        Resurrectable newResurrectable = newObj.GetComponent<Resurrectable>();
        newResurrectable.Resurrect();

        playerHealth.Damage(resurrectable.healthCost);

        anim.SetTrigger("resurrect");
    }
}
