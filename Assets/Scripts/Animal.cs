using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Animal : MonoBehaviour
{
    private Health health;
    private Animator anim;

    public float healthDecayPerSecond;

    public GrowthGrid grid;

    public int deathRegrowthRadius;
    public int moveRegrowthRadius;

    private AudioSource soundFX;

    public float corpseStayTime;

    private void Awake()
    {
        health = GetComponent<Health>();
        anim = GetComponentInChildren<Animator>();

        anim.SetTrigger("resurrect");
        
        health.OnDeath += Die;
    }
    private void Start()
    {
        soundFX = GetComponent<AudioSource>();
        if (grid == null)
        {
            grid = GrowthGrid.ActiveGrid;
        }
    }

    private void Update()
    {
        health.Damage(healthDecayPerSecond * Time.deltaTime);

        //leave a trail starting suitably far back so as to not influence the current direction of movement
        grid.SetBurstAtWorldSpacePosition(transform.position - (transform.forward * (1 + moveRegrowthRadius) * grid.cellSize), moveRegrowthRadius, 1f);

        if(Random.Range(0f, 1000f) < 1f)
        {
            anim.SetTrigger("idle_action");
        }
    }

    private void Die(GameObject obj)
    {
        anim.SetTrigger("die");
            
        grid.SetBurstAtWorldSpacePosition(transform.position, deathRegrowthRadius, 1f);
        //destroy all animal behaviour scripts immediately to prevent further shenanigans, but keep the corpse around for a while;
        soundFX.Stop();
        foreach(MonoBehaviour script in GetComponents<MonoBehaviour>())
        {
            Destroy(script);
            
        }
        Destroy(obj, corpseStayTime);
    }
}
