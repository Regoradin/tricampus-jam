using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowthGridBurster : MonoBehaviour
{
    public GrowthGrid grid;
    public int x;
    public int y;
    public int radius;
    public float val = 1f;

    public Transform debug;

    private void Update()
    {
        grid.SetBurstAtWorldSpacePosition(debug.position, radius, val);
    }
    
    public void Burst()
    {
        grid.SetBurst(x, y, radius, val);
    }
}
