using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AnimalMovement : MonoBehaviour
{
    private NavMeshAgent navAgent;

    public int maxSearchRadius;
    public float navigationOvershoot;

    private Animal animal;

    private void Awake()
    {
        navAgent = GetComponent<NavMeshAgent>();
        animal = GetComponent<Animal>();
    }

    private void Update()
    {
        if (animal.grid.GetGridValueAtWorldSpacePosition(transform.position) > 0)
        {
            SetDestinationAwayFromGrowth();            
        }

        float horizontalVelocityRelativeToCamera = Camera.main.transform.InverseTransformVector(navAgent.velocity).x;
        transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x) * Mathf.Sign(horizontalVelocityRelativeToCamera),
                                           transform.localScale.y,
                                           transform.localScale.z);
    }

    private void SetDestinationAwayFromGrowth()
    {
        Vector3 nearestUngrownPosition = animal.grid.GetWorldPositionFromGridPosition(FindNearestUngrownGridCell());
        Vector3 targetPosition = nearestUngrownPosition + (nearestUngrownPosition - transform.position).normalized * navigationOvershoot;

        navAgent.SetDestination(targetPosition);
        // Debug.Log("Setting : " + targetPosition);
        // Debug.DrawRay(targetPosition, Vector3.up * 10, Color.blue);
    }

    private Vector2Int FindNearestUngrownGridCell()
    {
        Vector2Int gridPosition = animal.grid.GetGridCoordFromWorldSpacePosition(transform.position);

        Vector2Int targetPosition = gridPosition;

        int radius = 1;
        while (radius < maxSearchRadius)
        {
            for(int i = -radius; i <= radius; i++)
            {
                Vector2Int[] positionsToCheck = new Vector2Int[4]
                {
                    new Vector2Int(radius + gridPosition.x, i + gridPosition.y),
                    new Vector2Int(i + gridPosition.x, radius + gridPosition.y),
                    new Vector2Int(-radius + gridPosition.x, i + gridPosition.y),
                    new Vector2Int(i + gridPosition.x, -radius + gridPosition.y)
                };

                foreach(Vector2Int position in positionsToCheck)
                {
                    if (CheckPosition(position))
                    {
                        return position;
                    }
                }
            }
            radius++;
        }
        Debug.LogWarning(name + " couldn't find good target position");
        return gridPosition + new Vector2Int(1, 1);
    }

    private bool CheckPosition(Vector2Int position)
    {
        return (animal.grid.IsValidCell(position) &&
                animal.grid.GetGridValueAtGridPosition(position) == 0);
    }
}
