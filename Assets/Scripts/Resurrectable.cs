using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resurrectable : MonoBehaviour
{
    public float healthCost;

    public void Resurrect()
    {
        //to ensure no double resurrection shenanigans
        GetComponentInChildren<Animator>().SetTrigger("resurrect");
        Destroy(this);
    }
}
