using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowthDecayGridUpdater : GridUpdater
{
    public float decayPerSecond;
    public int adjacentCellsRequiredToNotDecay;

    public float decayNoiseMagnitude;
    
    public override float[,] UpdateGrid(float[,] grid)
    {
        int width = grid.GetLength(0);
        int height = grid.GetLength(1);
        
        float[,] newGrid = new float[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                newGrid[x,y] = grid[x,y];

                if (newGrid[x,y] > 0)
                {
                    int adjacentCellsAboveZero = CalculateAdjacentCellsAboveZero(x, y, grid);

                    if (adjacentCellsAboveZero < adjacentCellsRequiredToNotDecay)
                    {
                        newGrid[x,y] -= Mathf.Max(0, (decayPerSecond + Random.Range(-decayNoiseMagnitude, decayNoiseMagnitude)) * Time.deltaTime);
                    }
                }
            }
        }

        return newGrid;
    }

    private int CalculateAdjacentCellsAboveZero(int x, int y, float[,] grid)
    {
        int adjacentCellsAboveZero = 0;
        for (int i = -1; i<=1; i++)
        {
            for (int j = -1; j<=1; j++)
            {
                //Look at all directly adjacent cells, but not diagonals and not this cell
                if (Mathf.Abs(i) != Mathf.Abs(j))
                {
                    try
                    {
                        if (grid[x + i, y+j] > 0)
                        {
                            adjacentCellsAboveZero++;
                        }
                    }
                    catch(System.IndexOutOfRangeException)
                    {
                        //treat off grid edges as above 0
                        adjacentCellsAboveZero++;
                    }
                }
            }
        }
        return adjacentCellsAboveZero;
    }
}
