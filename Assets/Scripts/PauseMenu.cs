using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    private bool paused = false;

    public GameObject pauseUI;

    private void Awake()
    {
        SetPaused(false);
    }
    
    private void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            SetPaused(!paused);
        }
    }

    public void SetPaused(bool pause)
    {
        paused = pause;
        
        pauseUI.SetActive(paused);
        
        Time.timeScale = paused ? 0f: 1f;
    }
}
