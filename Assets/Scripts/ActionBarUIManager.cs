using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ActionBarUIManager : MonoBehaviour
{
    [Header("These lists are to match resurrectables to icons")]
    public List<Resurrectable> resurrectables;
    public List<Sprite> icons;

    public GameObject iconPrefab;
    public GameObject iconHighlighter;

    public ResurrectionAbility resurrectionAbility;

    private List<GameObject> createdIcons;

    private void Awake()
    {
        createdIcons = new List<GameObject>();
        
        foreach (Resurrectable resurrectable in resurrectionAbility.availableResurrectables)
        {
            GameObject newIcon = Instantiate(iconPrefab);
            newIcon.transform.SetParent(transform);
            newIcon.GetComponent<Image>().sprite = icons[resurrectables.IndexOf(resurrectable)];

            createdIcons.Add(newIcon);
        }
    }

    private void Update()
    {
        //set size to be correct

        iconHighlighter.transform.position = createdIcons[resurrectionAbility.selectedResurrectableIndex].transform.position;
    }
}
