using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Animator anim;
    private GrowthGrid grid;
    private Health health;

    public float offGrowthDamagePerSecond;
    public int initialBurstRadius;

    public static Player ActivePlayer;

    public GameObject deathScreen;

    private void Awake()
    {
        anim = GetComponentInChildren<Animator>();
        health = GetComponent<Health>();
        health.OnDeath += Die;
        deathScreen.SetActive(false);

        Player.ActivePlayer = this;
    }
    private void Start()
    {
        grid = GrowthGrid.ActiveGrid;

        grid.SetBurstAtWorldSpacePosition(transform.position, initialBurstRadius, 1f);
    }

    private void Update()
    {
        if (grid.GetGridValueAtWorldSpacePosition(transform.position) <= 0)
        {
            health.Damage(offGrowthDamagePerSecond * Time.deltaTime);
        }
    }

    private void Die(GameObject obj)
    {
        anim.SetTrigger("die");

        Debug.Log("You Lose :(");

        deathScreen.SetActive(true);
        Time.timeScale = 0f;
        
        foreach(MonoBehaviour script in GetComponents<MonoBehaviour>())
        {
            Destroy(script);
        }
    }
}
