using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowthGrid : MonoBehaviour
{
    public int gridCount;
    public float cellSize;

    public GridUpdater gridUpdater;

    private float[,] grid;

    public GameObject gridTilePrefab;

    private Renderer[,] gridRenderers;

    //the lowest budget singleton possible
    public static GrowthGrid ActiveGrid;

    private void Awake()
    {
        InitializeGrid();
        GrowthGrid.ActiveGrid = this;
    }

    private void Update()
    {
        UpdateGridState();
        DrawGrid();
    }

    private void OnDrawGizmos()
    {
        if (Application.isPlaying)
        {
            DrawDebugGrid();
        }
    }

    private void InitializeGrid()
    {
        grid = new float[gridCount, gridCount];
        gridRenderers = new Renderer[gridCount, gridCount];
        for (int i = 0; i < gridCount; i++)
        {
            for (int j = 0; j < gridCount; j++)
            {
                //grid[i,j] = (float)(i + j)/ (2 * gridCount);
                grid[i,j] = 0;

                Vector3 gridCubeCenter = (new Vector3(i, 0, j) * cellSize) + (Vector3.one * 0.5f * cellSize);
                gridCubeCenter += transform.position;
                GameObject tile = Instantiate(gridTilePrefab, gridCubeCenter, Quaternion.identity, transform);
                tile.transform.localScale = Vector3.one * cellSize;
                gridRenderers[i,j] = tile.GetComponent<Renderer>();
            }
       }
    }

    private void UpdateGridState()
    {
        grid = gridUpdater.UpdateGrid(grid);
    }

    private void DrawGrid()
    {
        for (int x = 0; x < gridCount; x++)
        {
            for (int y = 0; y < gridCount; y++)
            {
                gridRenderers[x,y].material.SetFloat("Vector1_bdd48f0171e04e90bd5dc6060c94cf49", GetGridValueAtGridPosition(x, y));
                //clean and readable variable names. Thanks Unity.
            }
        }
        return;
    }

    private void DrawDebugGrid()
    {
        for (int i = 0; i < gridCount; i++)
        {
            for (int j = 0; j < gridCount; j++)
            {
                Gizmos.color = Color.Lerp(Color.red, Color.green, grid[i,j]);

                //indices refer to the corneWireCube wa center, so to be offset
                Vector3 gridCubeCenter = (new Vector3(i, 0, j) * cellSize) + (Vector3.one * 0.5f * cellSize);
                gridCubeCenter += transform.position;
                //size is slightly smaller to prevent overlap
                Vector3 gridSize = Vector3.one * (cellSize - 0.1f);
                Gizmos.DrawWireCube(gridCubeCenter, gridSize);
            }
        }
    }

    public bool IsValidCell(Vector2Int gridPosition)
    {
        return (gridPosition.x >= 0 && gridPosition.x < gridCount &&
                gridPosition.y >= 0 && gridPosition.y < gridCount);
    }

    public float GetGridValueAtGridPosition(int x, int y)
    {
        if (IsValidCell(new Vector2Int(x, y)))
        {
            return grid[x,y];            
        }
        return -1;
    }
    public float GetGridValueAtGridPosition(Vector2Int gridPosition)
    {
        return grid[gridPosition.x, gridPosition.y];
    }

    public float GetGridValueAtWorldSpacePosition(Vector3 position)
    {
        Vector2Int gridCoord = GetGridCoordFromWorldSpacePosition(position);
        return GetGridValueAtGridPosition(gridCoord.x, gridCoord.y);
    }

    public void SetCell(int x, int y, float val)
    {
        if (IsValidCell(new Vector2Int(x, y)))
        {
            grid[x,y] = val;            
        }
    }
    public void SetCellAtWorldSpacePosition(Vector3 position, float val)
    {
        Vector2Int gridCoord = GetGridCoordFromWorldSpacePosition(position);
        SetCell(gridCoord.x, gridCoord.y, val);
    }
    
    public void SetBurst(int x, int y, int radius, float val)
    {
        if (radius < 0)
        {
            throw new System.ArgumentOutOfRangeException("radius", radius, "Burst cannot have negative radius: " + radius);
        }
        for (int i = -radius; i <= radius; i++)
        {
            for (int j = -radius + Mathf.Abs(i); j <= radius - Mathf.Abs(i); j++)
            {
                SetCell(x + i, y + j, val);
            }
        }
    }
    public void SetBurstAtWorldSpacePosition(Vector3 position, int radius, float val)
    {
        Vector2Int gridCoord = GetGridCoordFromWorldSpacePosition(position);
        SetBurst(gridCoord.x, gridCoord.y, radius, val);
    }

    public void MakeDeltaBurst(int x, int y, int radius, float delta)
    {
        if (radius < 0)
        {
            throw new System.ArgumentOutOfRangeException("radius", radius, "Burst cannot have negative radius: " + radius);
        }
        for (int i = -radius; i <= radius; i++)
        {
            for (int j = -radius + Mathf.Abs(i); j <= radius - Mathf.Abs(i); j++)
            {
                SetCell(x + i, y + j, GetGridValueAtGridPosition(x + i, y + j) + delta);
            }
        }        
    }
    public void MakeDeltaBurstAtWorldSpacePosition(Vector3 position, int radius, float delta)
    {
        Vector2Int gridCoord = GetGridCoordFromWorldSpacePosition(position);
        MakeDeltaBurst(gridCoord.x, gridCoord.y, radius, delta);
    }

    public Vector2Int GetGridCoordFromWorldSpacePosition(Vector3 position)
    {
        //account for grid offset in world space
        position -= transform.position;
        position /= cellSize;

        return new Vector2Int((int)position.x, (int)position.z);
    }

    public Vector3 GetWorldPositionFromGridPosition(Vector2Int position)
    {
        return new Vector3(position.x * cellSize + transform.position.x,
                           transform.position.y,
                           position.y * cellSize + transform.position.z);
    }
}
