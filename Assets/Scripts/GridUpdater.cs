using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GridUpdater : MonoBehaviour
{
    public abstract float[,] UpdateGrid(float[,] grid);
}
