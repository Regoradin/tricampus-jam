using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public float maxHealth;
    public float health { get; private set; }

    public delegate void GameObjFunc(GameObject obj);
    public event GameObjFunc OnDeath;

    public Image healthBar;

    private void Awake()
    {
        health = maxHealth;
    }

    public void Damage(float damageDealt)
    {
        health -= damageDealt;

        UpdateHealthBar();
        
        if (health <= 0)
        {
            Die();
        }
    }

    public void Heal(float healthHealed)
    {
        health += healthHealed;
        health = Mathf.Min(health, maxHealth);

        UpdateHealthBar();
    }

    private void Die()
    {
        OnDeath(gameObject);
    }

    private void UpdateHealthBar()
    {
        healthBar.fillAmount = health/maxHealth;
    }
}
