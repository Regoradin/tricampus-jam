using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public GrowthGrid grid;

    public int pulseRadius;
    [Range(0, 1)]
    public float pulseValue;
    public float pulseTime;

    private float pulseTimeRemaining;

    public float regrownTimeNeededToKill;
    private float regrownTimeRemaining;

    private void Awake()
    {
        pulseTimeRemaining = pulseTime;
        regrownTimeRemaining = regrownTimeNeededToKill;
    }

    private void Start()
    {
        if (grid == null)
        {
            grid = GrowthGrid.ActiveGrid;
        }
    }

    private void Update()
    {
        pulseTimeRemaining -= Time.deltaTime;
        if (pulseTimeRemaining < 0)
        {
            pulseTimeRemaining = pulseTime;
            Pulse();
        }

        if (grid.GetGridValueAtWorldSpacePosition(transform.position) > 0)
        {
            regrownTimeRemaining -= Time.deltaTime;
        }
        if (regrownTimeRemaining <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void Pulse()
    {
        grid.MakeDeltaBurstAtWorldSpacePosition(transform.position, pulseRadius, -pulseValue);
    }

}
