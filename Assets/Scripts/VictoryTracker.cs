using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryTracker : MonoBehaviour
{
    [Range(0, 1)]
    public float requiredGrownPercentage;
    public float currentGrownPercentage {get; private set;}

    private GrowthGrid grid;

    public Image victoryTrackerCircle;

    public GameObject victoryScreen;

    private void Start()
    {
        grid = GrowthGrid.ActiveGrid;

        victoryScreen.SetActive(false);
    }

    private void Update()
    {
        int totalCells = 0;
        int grownCells = 0;
        
        for(int x = 0; x < grid.gridCount; x++)
        {
            for(int y = 0; y < grid.gridCount; y++)
            {
                totalCells++;
                if (grid.GetGridValueAtGridPosition(x, y) > 0)
                {
                    grownCells++;
                }
            }
        }

        currentGrownPercentage = (float)grownCells/(float)totalCells;

        victoryTrackerCircle.fillAmount = currentGrownPercentage / requiredGrownPercentage;

        if (currentGrownPercentage >= requiredGrownPercentage)
        {
            Win();
        }
    }

    private void Win()
    {
        Debug.Log("You won!");

        victoryScreen.SetActive(true);
        Time.timeScale = 0f;        
    }
}
