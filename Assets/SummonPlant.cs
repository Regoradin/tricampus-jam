using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SummonPlant : MonoBehaviour
{
    public GameObject plantObject;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //if [player adds plant]

        Instantiate(plantObject, transform.position, Quaternion.identity);
    }
}
