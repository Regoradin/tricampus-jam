using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private CharacterController controller;
    private Animator anim;

    public float speed = 20f;
    //public float rotationSpeed;

    private void Awake()
    {
        controller = GetComponent<CharacterController>();
        anim = GetComponentInChildren<Animator>();
    }
    
    void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        Vector3 direction = new Vector3(horizontal, 0f, vertical);

        if (direction.magnitude >= 0.1f)
        {
            controller.Move(direction * speed * Time.deltaTime);
            //transform.forward = direction;
            //Quaternion toRotation = Quaternion.LookRotation(direction, Vector3.up);
            //transform.rotation = Quaternion.RotateTowards(transform.rotation, toRotation, rotationSpeed * Time.deltaTime);
        }

        float horizontalVelocityRelativeToCamera = Camera.main.transform.InverseTransformVector(direction).x;
        if (direction.magnitude > 0 && horizontalVelocityRelativeToCamera == 0)
        {
            //hack to make the animation play anyway when moving in the vertical direction
            horizontalVelocityRelativeToCamera = 1;
        }
        anim.SetFloat("speed", horizontalVelocityRelativeToCamera);
    }
}
